package pe.uni.armandollueng.radiobutton;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

public class RadioButtonActivity extends AppCompatActivity {
    ImageView imageView;
    RadioButton radioButton1;
    RadioButton radioButton2;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);

        imageView = findViewById(R.id.image_view);
        radioButton1 =  findViewById(R.id.radio_button_logo_1);
        radioButton2 = findViewById(R.id.radio_button_logo_2);
        button = findViewById(R.id.button_1);

        button.setOnClickListener(v -> {
            if(radioButton1.isChecked()){
                imageView.setImageResource(R.drawable.b);
                return;
            }

            if(radioButton2.isChecked()){
                imageView.setImageResource(R.drawable.c);
            }
        });
    }
}