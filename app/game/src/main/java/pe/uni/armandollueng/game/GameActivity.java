package pe.uni.armandollueng.game;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    TextView textViewLastAttempt, textViewRemainingAttempt, textViewHint;
    EditText editTextGuessedNumber;
    Button buttonGuessNumber;

    Boolean twoDigits, threeDigits, fourDigits;

   Random r = new Random();

   int remainingAttempts = 10;
   int random;

   ArrayList<Integer> attemptsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        textViewLastAttempt = findViewById(R.id.text_view_last_attempt);
        textViewRemainingAttempt =  findViewById(R.id.text_view_remaining_attempts);
        textViewHint = findViewById(R.id.text_view_hint);

        editTextGuessedNumber = findViewById(R.id.edit_text_guessed_number);

        buttonGuessNumber = findViewById(R.id.button_guess_number);

        twoDigits = getIntent().getBooleanExtra("TWO",false);
        threeDigits = getIntent().getBooleanExtra("THREE",false);
        fourDigits = getIntent().getBooleanExtra("FOUR",false);

        if(twoDigits){
            random = 10 + r.nextInt(90);//lim inf + r.nextInt(lindup - liminf)
        }
        if(threeDigits){
            random = 100 + r.nextInt(900);//lim inf + r.nextInt(lindup - liminf)
        }
        if(fourDigits){
            random = 1000 + r.nextInt(9000);//lim inf + r.nextInt(lindup - liminf)
        }

        buttonGuessNumber.setOnClickListener(v -> {
            String sNumberGuess = editTextGuessedNumber.getText().toString();
            if(sNumberGuess.equals("")){
                Toast.makeText(GameActivity.this, R.string.button_guess_number_msg,Toast.LENGTH_LONG).show();
                return;
            }

            remainingAttempts--;

            int iNumberGuess = Integer.parseInt(sNumberGuess);

            attemptsList.add(iNumberGuess);

            textViewLastAttempt.setVisibility(View.VISIBLE);
            textViewRemainingAttempt.setVisibility(View.VISIBLE);
            textViewHint.setVisibility(View.VISIBLE);

            Resources res = getResources();
            textViewLastAttempt.setText(String.format(res.getString(R.string.text_view_last_attempt), sNumberGuess));
            textViewRemainingAttempt.setText(String.format(res.getString(R.string.text_view_remaining_attempt), remainingAttempts));

            if(random == iNumberGuess){
                textViewHint.setText(R.string.text_view_hint_guessed);
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.dialog_msg_win), random, (10 - remainingAttempts),attemptsList));
                builder.setPositiveButton("Si", (dialog, which) -> {
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton("No", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());//matar el proceso
                    System.exit(1);
                });
                builder.create().show();
            }
            if(random < iNumberGuess){
                textViewHint.setText(R.string.text_view_hint_decrease);
            }
            if(random > iNumberGuess){
                textViewHint.setText(R.string.text_view_hint_increase);
            }

            if(remainingAttempts == 0){
                textViewHint.setText(R.string.text_view_hint_lose);
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.dialog_msg_lose), random,attemptsList));
                builder.setPositiveButton("Si", (dialog, which) -> {
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton("No", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());//matar el proceso
                    System.exit(1);
                });
                builder.create().show();
            }

            editTextGuessedNumber.setText("");

        });

    }
}