package pe.uni.armandollueng.game;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    ImageView imageViewSplash;
    TextView textViewSplash;

    Animation animationImage, animationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageViewSplash = findViewById(R.id.image_view_splash);
        textViewSplash = findViewById(R.id.text_view_splash);

        //vincular las animaciones de la carpeta anim
        animationImage = AnimationUtils.loadAnimation(this,R.anim.animation);
        animationText = AnimationUtils.loadAnimation(this,R.anim.text_animation);

        //vincular las animaciones con los componentes
        imageViewSplash.setAnimation(animationImage);
        textViewSplash.setAnimation(animationText);

        //timer de cuenta regresva
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();

    }
}