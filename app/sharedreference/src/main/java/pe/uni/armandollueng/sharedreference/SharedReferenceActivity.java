package pe.uni.armandollueng.sharedreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SharedReferenceActivity extends AppCompatActivity {

    EditText editTextName;
    EditText editTextMessage;
    Button button;
    CheckBox checkBox;
    SharedPreferences sharedPreferences;
    String name;
    String message;
    Boolean isCheked;
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_reference);

        editTextMessage = findViewById(R.id.edit_text_message);
        editTextName = findViewById(R.id.edit_text_name);
        button = findViewById(R.id.button);
        checkBox = findViewById(R.id.check_box);

        button.setOnClickListener(v -> {
            counter++;
            button.setText(String.valueOf(counter));
        });

        replayData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isCheked = checkBox.isChecked();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", name);
        editor.putString("message", message);
        editor.putInt("counter", counter);
        editor.putBoolean("remember", isCheked);
        editor.apply();//tambien commit

        Toast.makeText(getApplicationContext(), "tus datos estan guardados", Toast.LENGTH_LONG).show();
    }

    private void replayData(){
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);//misma instancia
        name = sharedPreferences.getString("name", null);
        message = sharedPreferences.getString("message", null);
        counter = sharedPreferences.getInt("counter", 0);
        isCheked = sharedPreferences.getBoolean("remember", false);

        editTextName.setText(name);
        editTextMessage.setText(message);
        button.setText(String.valueOf(counter));
        checkBox.setChecked(isCheked);
    }
}