package pe.uni.armandollueng.togglebutton;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

public class ToggleButtonActivity extends AppCompatActivity {

    ImageView imageView;
    ToggleButton toggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_button);

        imageView = findViewById(R.id.image_view);
        toggleButton = findViewById(R.id.toggle_button);

        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                imageView.setVisibility(View.VISIBLE);
            }else{
                imageView.setVisibility(View.INVISIBLE);
            }
        });

    }
}