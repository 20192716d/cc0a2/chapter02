package pe.uni.armandollueng.gridview;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class GridViewActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView = findViewById(R.id.grid_view);
        fillArray();
        //crear el adapter y vincularlo

        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG).show());
    }

    private void fillArray(){
        text.add("Conejo");
        text.add("Gato");
        text.add("Leon");
        text.add("Pajarito");
        text.add("Perro");

        image.add(R.drawable.conejo);
        image.add(R.drawable.gato);
        image.add(R.drawable.leon);
        image.add(R.drawable.pajarito);
        image.add(R.drawable.perro);
    }
}