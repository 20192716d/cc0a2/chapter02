package pe.uni.armandollueng.linearlayaout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
/*
Layaout es la interfaz basica principal
hay layaout con restricciones y otros que no
 */

public class LinearLayaoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_layaout);
    }
}