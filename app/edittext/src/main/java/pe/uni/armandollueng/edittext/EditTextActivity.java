package pe.uni.armandollueng.edittext;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class EditTextActivity extends AppCompatActivity {

    EditText editText;
    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text);

        editText= findViewById(R.id.edit_text_1);
        button = findViewById(R.id.button_1);
        textView = findViewById(R.id.text_view_1);

        button.setOnClickListener(v -> {
            String name = editText.getText().toString();
            textView.setText(name);
        });
    }
}