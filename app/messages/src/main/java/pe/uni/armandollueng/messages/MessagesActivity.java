package pe.uni.armandollueng.messages;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class MessagesActivity extends AppCompatActivity {

    Button  buttonToast, buttonSnackBar, buttonDialog;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        buttonToast = findViewById(R.id.button_message_1);
        buttonSnackBar =  findViewById(R.id.button_message_2);
        buttonDialog = findViewById(R.id.button_message_3);
        linearLayout = findViewById(R.id.linear_layout);
        //getaplicationContext-> cotexto de toda la aplicacion//muy grande
        buttonToast.setOnClickListener(v -> Toast.makeText(getApplicationContext(), R.string.msg_toast,Toast.LENGTH_LONG).show());

        buttonSnackBar.setOnClickListener(v -> {
            //contexto solo al linearlayaout (grupo padre) // mas seguro
            Snackbar.make(linearLayout, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE).setAction("cerrar", v1 -> {

            }).show();
        });

        buttonDialog.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(R.string.dialog_title).
                    setMessage(R.string.dialog_msg).
                    setNegativeButton(R.string.No, (dialog, which) -> {

                    }).setPositiveButton(R.string.yes, (dialog, which) -> {

                    }).show();
            alertDialog.create();
        });
    }
}