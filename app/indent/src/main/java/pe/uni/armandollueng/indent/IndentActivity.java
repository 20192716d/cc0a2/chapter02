package pe.uni.armandollueng.indent;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class IndentActivity extends AppCompatActivity {

    Button button;
    EditText editText, editNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indent);

        button = findViewById(R.id.button_intent);
        editText = findViewById(R.id.edit_text);
        editNumber = findViewById(R.id.edit_number);

        button.setOnClickListener(v -> {
            String sText = editText.getText().toString();
            String sNumber = editNumber.getText().toString();

            Intent intent = new Intent(IndentActivity.this, SecondActivity.class);

            //TEXT->keyword
            //sText->Valor
            intent.putExtra("TEXT", sText);

            if(!sNumber.equals("")){
                int number = Integer.parseInt(sNumber);
                //NUMBER->keyword
                //number->Valor
                intent.putExtra("NUMBER", number);
            }

            startActivity(intent);
            //finish();//para cerrar el activity
        });
    }
}