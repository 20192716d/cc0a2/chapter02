package pe.uni.armandollueng.indent;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    TextView textViewText,textViewNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textViewText =  findViewById(R.id.text_view_text);
        textViewNumber = findViewById(R.id.text_view_number);

        Intent intent = getIntent(); // invocar el intent de la primera aplicacion
        String text = intent.getStringExtra("TEXT");
        int number = intent.getIntExtra("NUMBER", 0);//0 por defecto

        textViewText.setText(text);
        textViewNumber.setText(String.valueOf(number));
    }

}