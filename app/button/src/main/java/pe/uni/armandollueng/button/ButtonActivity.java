package pe.uni.armandollueng.button;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ButtonActivity extends AppCompatActivity {
    Button boton1;
    Button boton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);
        boton1 = findViewById(R.id.button1);
        boton2 = findViewById(R.id.button2);

        boton1.setOnClickListener(v -> {
            boton1.setBackgroundColor(Color.RED);
            boton2.setVisibility(View.INVISIBLE);
        });
    }


}