package pe.uni.armandollueng.listview;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class List_view extends AppCompatActivity {

    ListView list_view;
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        list_view = findViewById(R.id.list_view);
        String [] countries = getResources().getStringArray(R.array.countries);
        //adaptador
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countries);
        //anexmos el listview con el adapter
        list_view.setAdapter(arrayAdapter);

        list_view.setOnItemClickListener((parent, view, position, id) -> {
            String country = parent.getItemAtPosition(position).toString();
            Toast.makeText(getApplicationContext(),
                    String.format(getResources().getString(R.string.Toast_msg), country), Toast.LENGTH_LONG).show();
        });

    }
}